## ChatComponent
```plantuml
@startuml
component Chat {

class ChatRESTFacade {
  + addMessage(MessageDTO): void
  + addToChat(Long userId, Long chatId): void
  + createChat(ChatDTO): Long chatId
  + getChat(Long chatId): ChatDTO
  + getUsersChats(Long userId): List<ChatDTO>
  + getMessages(Long chatId): List<Message>
}

class Chat {
  - Long id
  - String chatName
  - Enum chatType
  - User ChatOwner
  - List<User> users
  - List<Message> messages
  + addUser(User user): void
  + addMessage(Message): void
}

interface ChatService {
  + addMessage(Message): void
  + addToChat(Long userId, Long chatId): void
  + createChat(Chat): Long chatId
  + getChat(Long chatId): Chat
  + getUsersChats(Long userId): List<Chat>
  + getMessages(Long chatId): List<Message>
}

class ChatComponentBusinessLogic {
  + addMessage(Message): void
  + addToChat(Long userId, Long chatId): void
  + createChat(Chat): Long chatId
  + getChat(Long chatId): Chat
  + getUsersChats(Long userId): List<Chat>
  + getMessages(Long chatId): List<Message>
}

ChatComponentBusinessLogic -|> ChatService
ChatRESTFacade "1"-up-"1" ChatService
Chat -up- ChatComponentBusinessLogic: manages

}
@enduml

```

## UserComponent
```plantuml
@startuml
component User {
  class UserRESTFacade {
  	+ getUserChats(Long userId): List<MessageDTO>
	+ getUser(Long userId): UserDTO
    + createUser(UserDTO): Long userId
  }

  interface UserService {
  	+ getUserChats(Long userId): List<Message>
	+ getUser(Long userId): User
    + createUser(User): Long userId
  }
  
  class UserComponentBusinessLogic {
  	+ getUserChats(Long userId): List<Message>
	+ getUser(Long userId): User
    + createUser(User): Long userId
  }

  class User {
    - Long id
    - String userName
    - Email email
  }
  
  UserComponentBusinessLogic -|> UserService
  UserRESTFacade "1"-up-"1" UserService
  User -up- UserComponentBusinessLogic: manages
}
@enduml
```

## MessageComponent
```plantuml
@startuml
component Message {
  interface MessageService {
  	+ createMessage(Message): long messageId
  }
  
  class MessageComponentBusinessLogic {
  	+ createMessage(Message): long messageId
  }

  class Message {
    - Long messageId
    - Long timestamp
    - User sender
    - Chat chat
    - Date date
    - String messageText
  }
  
  MessageComponentBusinessLogic -|> MessageService
  MessageComponentBusinessLogic -- Message: manages
}
@enduml
```

## NodificationHandler
```plantuml
@startuml
component NodificationHandler {
  class NodificationRESTFacade {
  	+ getNodifications(Long userId): List<NodificationDTO>
  }

  interface NodificationService {
  	+ sendMessage(Long messageId, Long chatId): void
    + getNodifications(Long userId): List<Nodification>
  }
  
  class NodificationComponentBusinessLogic {
  	+ sendMessage(Long messageId, Long chatId): void
    + getNodifications(Long userId): List<Nodification>
  }

  class Nodification {
  	- Long id
    - Message
    - Chat
  }
  
  NodificationComponentBusinessLogic -|> NodificationService
  NodificationComponentBusinessLogic -- Nodification: manages
  NodificationRESTFacade "1"-up-"1" NodificationService
}
@enduml
```
