### new classes
```plantuml
interface Game
interface Controller
interface Display

interface MessageListener

class GameStub {
	- Marshaller
    - Connection
}
class ControllerStub {
	- Marshaller
    - Connection
}
class DisplayStub {
	- Marshaller
    - Connection
}


GameStub -up-|> Game
ControllerStub -up-|> Controller
DisplayStub -up-|> Display

GameStub --|> MessageListener
ControllerStub --|> MessageListener
```

## link
```plantuml
interface MessageListener  {
	void onMessage(String)
}

interface MessageProvider {
	void addMessageListener(MessageListener)
}

interface Connection {
	void cancel()
}

interface Connector {
	void run()
    void addConnection()
    void addListen()
    void addConnection(SocketAddress) 
    void addListen(SocketAddress)
    void stop()
    void addConnectionListener(ConnectionListener)
}

interface ConnectionListener {
	void onConnection(Connection)
}

Connection -up-|> MessageListener
Connection -up-|> MessageProvider

Connector *-- ConnectionListener

Connection "*" -- "1" Connector
```